<?php
function tentukan_nilai($number)
{
    //  kode disini
    $uotput = " ";
    if ($number >= 98 && $number < 100) {
        $uotput .= "Sangat Baik" . "<br>";
    } elseif ($number >= 76 && $number < 97) {
        $uotput .= "Baik" . "<br>";
    } elseif ($number >= 67 && $number < 75) {
        $uotput .= "Cukup" . "<br>";
    } else {
        $uotput .= "Kurang" . "<br>";
    }
    return $uotput;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>